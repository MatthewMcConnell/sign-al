# Sign-al

Work from an exploration into the feasibility of a conversational Sign Language agent for my Honours Level Individual Project at the University of Glasgow. The most significant work is a 2D virtual agent that can sign any text given to it using the British Sign Language alphabet (this could easily be extended to sign using a full Sign Language). [See the signing virtual agent in action](https://youtu.be/7p8U6BFdHBI)

There are 3 main components to this repository. 
- The openpose keypoint sequence dictionary is processed by a script `src/training-video-processor.py`
- The sign language animation generation is within the `src/synthesis/` folder and uses data from the evaluation-text and training-data-keypoints folder.
- The sign language recognition exploration is in the experimental jupyter notebook `src/Sign_Recognition.ipynb`

Other folders and files include:
- `evaluation-text/` :: folder containing text to be part of an evaluation
- `evaluation-text/user.txt` :: the list of phrases used in the user evaluation
- `human/` :: folder containing the digital body part models
- `pip-package-requirements.txt` :: pip requirements file
- `training-data-keypoints/` :: folder to contain json dictionaries outputted by the training-video-processor script

## Build instructions

### Requirements

List the all of the pre-requisites software required to set up your project (e.g. compilers, packages, libraries, OS, hardware)

Requirements:
- Python 3
- OpenPose installation with a python build
  - Use of a GPU would be preferable for OpenPose but should not be essential
- Pip Package Requirements listed in `pip-package-requirements.txt`
- Tested on Linux (Arch Distro) with a GPU

### Build steps

1. Install and build the OpenPose library with a Python Build - instructions for this vary and can be found at the [OpenPose GitHub Repo](https://github.com/CMU-Perceptual-Computing-Lab/openpose)
2. Install the package requirements through `pip install -r pip-package-requirements.txt`

### Test steps

Some of the packages and installations like openpose have examples that you can run to test that everything was installed correctly. Otherwise by running the programs as described in [Usage](#usage-instructions) this also tests if your build and setup proceeded correctly.

## Usage Instructions

### Training Video Processor

1. `cd src`
2. `python training-video-processor.py --help` to see the arguments to provide
3. `python training-video-processor.py` adding any arguments necessary to run the script

Note that the video dataset is not provided. You can create your own simple dataset easily by creating folder containing a structure similar to the below. Note that the *signX* should be renamed to what the videos within that folder represent. For example if they contain videos of the sign 'car' then name the directory 'car'.

- Sign1
  - video1
  - video2
- Sign2
  - video1
- ...

### Sign Language Synthesis

1. `cd src/synthesis`
2. `python main.py`

### Sign Language Recognition

The exploration of the sign language recognition was developed on a google colab notebook. While it is structured, since it is an exploration it is not built for ease of use by other people. 

You can view and make a copy of the [colab notebook](https://colab.research.google.com/drive/1w9mhSUpwuGX1n5dJIfevH6jIpLjSdZAP). Or alternatively, you can access the local downloaded version of the notebook. However, since the notebook was developed in google colab it accesses data through google drive files.

I do not provide the video dataset and thus to use the ResNet model (CNN) you should create your own video dataset as detailed in [Training Video Processor](#training-video-processor). The openpose keypoints data for the LSTM can be found at the local file of `training-data-keypoints/max-training-v2.json`.

Given the two points above you will need to do some alterations to the jupyter notebook to get it to run on files which you create or use from my repository, whether this is locally or on google colab.