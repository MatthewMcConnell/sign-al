"""
    This script will process a directories of videos into openpose keypoints.
    Each directory should be named according to the what the videos within them are signing.
    For example if you had a directory containing videos that sign 'how are you?' then you should name the directory 'how are you?'

    This will be output into 1 flattened JSON file with the format of it described and displayed in a comment in the source file.

    There are various arguments to provide where you can tweak the behaviour of the script. You can see this in the command line
    using the --help flag.
"""

import argparse
import json
import os
import sys
from sys import platform

import cv2
import numpy
from tqdm import tqdm



# Flags
parser = argparse.ArgumentParser()
parser.add_argument(
    "--video_dirs", 
    default="../../alphabet-videos/letters", 
    help="Process directories of videos to produce openpose output data for."
)
parser.add_argument(
    "--accuracy", 
    default="max", 
    help="Choose an accuracy level from :: max | medium | low . Choosing a lower accuracy will increase the speed of the script"
)
parser.add_argument(
    "--output_path",
    default="../training-data-keypoints/training.json",
    help="Provide a path for where you would like the output json file to be."
)
parser.add_argument(
    "--openpose_model",
    default="../models/",
    help="The path to your openpose models directory."
)
parser.add_argument(
    "--openpose_library",
    default="/usr/local/python",
    help="The path to your openpose python installation."
)
args = parser.parse_known_args()

print()
print("Accuracy ->", args[0].accuracy) 
print("Video Directory ->", args[0].video_dirs)
print("Output Path ->", args[0].output_path)
print("OpenPose Model Directory ->", args[0].openpose_model)
print("OpenPose Model Directory ->", args[0].openpose_library)
print()




# Import Openpose (Windows/Ubuntu/OSX)
dir_path = os.path.dirname(os.path.realpath(__file__))
try:
    # Windows Import
    if platform == "win32":
        # Change these variables to point to the correct folder (Release/x64 etc.) 
        sys.path.append(dir_path + args[0].openpose_library);
        os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/../../x64/Release;' +  dir_path + '/../../bin;'
        import pyopenpose as op
    else:
        # Change these variables to point to the correct folder (Release/x64 etc.) 
        # sys.path.append('../../python');
        # If you run `make install` (default path is `/usr/local/python` for Ubuntu), 
        # you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
        sys.path.append(args[0].openpose_library)
        from openpose import pyopenpose as op
except ImportError as e:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
    raise e



params = dict()

# Setting the path to the openpose models folder
params["model_folder"] = args[0].openpose_model

# Doing openpose on hands too
params["hand"] = True

# Number of people allowed to be detected is capped at 1
params["number_people_max"] = 1

# range of [0,1]  where (0,0) would be the top-left corner of the image, and (1,1) the bottom-right one
params["keypoint_scale"] = 3

# Selecting a net resolution if specified, has the effect of making openpose run faster
if args[0].accuracy == "medium":
    params["net_resolution"] = "-1x240"
    params["hand_net_resolution"] = "240x240"
elif args[0].accuracy == "low":
    params["net_resolution"] = "-1x128"
    params["hand_net_resolution"] = "128x128"


# Form of the json output dictionary
# {
#     sign:
#         [
#             { # video
#                 body: [
#                     [frame],
#                     ...
#                 ],
#                 left_hand: [
#                     [frame],
#                     ...
#                 ]
#                 right_hand: [
#                     [frame],
#                     ...
#                 ]
#             },
#             ...
#         ],
#     ...
# }

try:
    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()

    output_json_object = dict()
    
    with os.scandir(args[0].video_dirs) as sign_directories:
        for sign_directory in tqdm(sign_directories, "Script Completion", len(os.listdir(args[0].video_dirs))):

            sign_videos_output_list = []

            with os.scandir(sign_directory) as videos:
                for video in tqdm(videos, sign_directory.name, len(os.listdir(sign_directory))):
                    input_video_path = args[0].video_dirs + "/" + sign_directory.name + "/" + video.name
                    input_video = cv2.VideoCapture(input_video_path)

                    openpose_video_output = dict([("body", []), ("left_hand", []), ("right_hand", [])])

                    while input_video.isOpened():
                        read, frame = input_video.read()

                        if not read:
                            break
                        
                        # Getting the openpose points from a frame
                        datum = op.Datum()
                        datum.cvInputData = frame
                        opWrapper.emplaceAndPop([datum])

                        # shape of pose array is an array with 25 points, each with 3 values (x coord, y coord, confidence)
                        upper_body_pose_keypoints = numpy.concatenate(
                            (datum.poseKeypoints[:, 0:8], datum.poseKeypoints[:, 15:19]),
                            axis=1
                        ).squeeze()

                        left_hand_keypoints = datum.handKeypoints[0].squeeze()
                        right_hand_keypoints = datum.handKeypoints[1].squeeze()

                        # Thresholding the confidence values so that they are more distinct a classifier
                        threshold_body = upper_body_pose_keypoints[:, 2] < 0.5
                        threshold_left_hand = left_hand_keypoints[:, 2] < 0.5
                        threshold_right_hand = right_hand_keypoints[:, 2] < 0.5

                        upper_body_pose_keypoints[threshold_body, 2] = 0
                        upper_body_pose_keypoints[numpy.logical_not(threshold_body), 2] = 1

                        left_hand_keypoints[threshold_left_hand, 2] = 0
                        left_hand_keypoints[numpy.logical_not(threshold_left_hand), 2] = 1

                        right_hand_keypoints[threshold_right_hand, 2] = 0
                        right_hand_keypoints[numpy.logical_not(threshold_right_hand), 2] = 1

                        # Changing to a list so that I can output it to JSON
                        openpose_video_output["body"].append(upper_body_pose_keypoints.tolist())
                        openpose_video_output["left_hand"].append(left_hand_keypoints.tolist())
                        openpose_video_output["right_hand"].append(right_hand_keypoints.tolist())

                    input_video.release()

                    sign_videos_output_list.append(openpose_video_output)

            output_json_object[sign_directory.name] = sign_videos_output_list

    with open(args[0].output_path, 'w') as outfile:
        json.dump(output_json_object, outfile)


    print("Done!")

except Exception as e:
    print(e)
    sys.exit(-1)
