"""
    Contains a Generic Class for a Body Part
"""

import pyglet

class BodyPart:
    """
        Represents a part of the agent body.

        Contains methods on repositioning and drawing a body part.
    """

    def __init__ (self, img_path):
        """
            Creates a single body part

            Params
            ------
            img_path : str
                file path to the body part image

            Returns
            -------
            BodyPart
        """

        # The sprite image of the body part
        self.sprite = pyglet.sprite.Sprite(pyglet.image.load(img_path))

    
    def draw (self):
        """ Draws the body part sprite. """
        self.sprite.draw()

    
