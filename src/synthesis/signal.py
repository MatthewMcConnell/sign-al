"""
    Module containing the main application class.
"""

import pyglet
from agent import Agent
from pose_processor import load_sign_dictionary, segment_pose, generate_text_to_sign


WIDTH, HEIGHT = None, None


class SignAl ():
    """
        Contains parameters and runs the animations.
    """

    def __init__ (self, res, model_dir, sign_dict_path, space_frame_delay=40, fps=40):
        """
            Constructor for animation runner with key parameters.

            Params
            ------
            res : str
                represents pixel width and height of the window e.g. "1920x1080"
            model_dir : str
                the directory containing the body parts
            sign_dict_path : str
                file path to the sign dictionary of openpose keypoints
            space_frame_delay : int, optional
                the number of frames to pause on if there is a space character in the text to sign
            fps : int, optional
                the frames per second that the animation should run at
        """

        global WIDTH, HEIGHT

        res = res.split('x')
        WIDTH, HEIGHT = int(res[0]), int(res[1])

        self.window = pyglet.window.Window(width=WIDTH, height=HEIGHT)

        self.sign_dictionary = load_sign_dictionary(sign_dict_path)

        self.agent = Agent(
            model_dir + "head.png",
            model_dir + "neck.png",
            model_dir + "body.png",
            model_dir + "upperarm.png",
            model_dir + "forearm.png",
            model_dir + "hand.png",
            model_dir + "finger.png"
        )

        self.delay = space_frame_delay
        self.frame_interval = 1 / fps

        self.animating = False
        self.label = pyglet.text.Label(
            "To play the next animation \n(or end the program if there are no animations left) \npress ENTER", 
            x=WIDTH/2, y=HEIGHT,
            anchor_x="center",
            anchor_y="top",
            width=WIDTH, 
            multiline=True,
            align="center"
        )


    def sign_text (self, text):
        self.animation = generate_text_to_sign(text, self.sign_dictionary, space_frame_delay=self.delay)

        self.frame_num = 0
        self.num_of_frames = len(self.animation["body"])

        pyglet.clock.schedule_interval(self.animate, self.frame_interval)

        self.animating = True
    

    def animate (self, dt):
        pose = segment_pose(
            self.animation["body"][self.frame_num], 
            self.animation["left_hand"][self.frame_num], 
            self.animation["right_hand"][self.frame_num]
        )

        self.agent.update(pose)

        self.frame_num += 1

        if self.frame_num == self.num_of_frames:
            pyglet.clock.unschedule(self.animate)
            self.animating = False

    

    

