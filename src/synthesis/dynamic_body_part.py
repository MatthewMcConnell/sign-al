"""
    A module that contains the dynamic body part class
"""

from math import sqrt, acos, degrees
import pyglet
import body_part
import signal

class DynamicBodyPart (body_part.BodyPart):
    """
        Represents a dynamic body part such as a limb that will
        aim to have an axis point which will be rotated such that
        the body part is drawn between 2 points.
    """

    def __init__ (self, img_path, scale_x=2.9):
        """
            Creates a dynamic body part with the anchor of the sprite being the top-middle of the sprite.

            Params
            ------
            img_path : str
                the file path to the image to be used as the sprite

            scale_x : float
                the scaling factor the body part should have if there are no width coordinates provided

            Returns
            -------
            DynamicBodyPart
        """
        super().__init__(img_path)

        self.sprite.image.anchor_x = self.sprite.width / 2
        self.sprite.image.anchor_y = self.sprite.height * 0.9 # using the top of the image

        self.scale_x = scale_x


    def update (self, coords):
        """
            Updates the position and scale of the body part.

            Params
            ------
            coords : tuple
                contains floats that represent (x1, y1, x2, y2)
                may optionally contain 2 extra coordinate pairs for width scaling
        """
        x1, y1, x2, y2 = coords[0][0] * signal.WIDTH, (1 - coords[0][1]) * signal.HEIGHT, coords[1][0] * signal.WIDTH, (1 - coords[1][1]) * signal.HEIGHT

        magnitude = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

        # if there is no difference in the coordinates then do not draw the body part
        if magnitude == 0:
            self.sprite.visible = False
            return
        else:
            self.sprite.visible = True


        ### ROTATING ###

        # by using a vertical (downward) vector of (0, -1) dot product is equal to the y distance
        dot_product = -1 * (y2 - y1)

        angle = degrees(acos(dot_product / magnitude))

        # if the anchor x is left with respect to the target x then reverse the angle
        if x1 - x2 < 0:
            angle *= -1

        ### SCALING ###

        # finding the vertical scale of the image to reach from (x1, y1) to (x2, y2)
        scale_y = magnitude / self.sprite.image.height * 1.2

        # If extra width coordinates are provided then find the x scale to use
        if len(coords) == 4:
            x3, y3, x4, y4 = coords[2][0] * signal.WIDTH, (1 - coords[2][1]) * signal.HEIGHT, coords[3][0] * signal.WIDTH, (1 - coords[3][1]) * signal.HEIGHT

            magnitude = sqrt((x3 - x4) ** 2 + (y3 - y4) ** 2)

            scale_x = magnitude / self.sprite.image.width * 1.4
        else:
            scale_x = self.scale_x


        self.sprite.update(x1, y1, rotation=angle, scale_x=scale_x, scale_y=scale_y)

        


        

