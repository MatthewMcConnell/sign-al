"""
    Contains the class of the full animated agent.
"""

import static_body_part
import dynamic_body_part

BODY_PART_NAMES = [
    "neck", 
    "head", 
    "body", 
    "upperarm-l", # bodypart-left/right
    "upperarm-r", 
    "forearm-l",
    "forearm-r",
    "hand-l",
    "hand-r",
]

FINGER_PART_NAMES = [
    "thumb-l-t", "thumb-l-m", "thumb-l-b", # finger-left/right-bottom/middle/top
    "thumb-r-t", "thumb-r-m", "thumb-r-b",
    "index-l-t", "index-l-m", "index-l-b", 
    "middle-l-t", "middle-l-m", "middle-l-b",
    "ring-l-t", "ring-l-m", "ring-l-b",
    "pinky-l-t", "pinky-l-m", "pinky-l-b",
    "index-r-t", "index-r-m", "index-r-b", # decided to have the right fingers render on top
    "middle-r-t", "middle-r-m", "middle-r-b",
    "ring-r-t", "ring-r-m", "ring-r-b",
    "pinky-r-t", "pinky-r-m", "pinky-r-b",
]

# Customised drawing order so that specific body parts are drawn on top of other body parts
DRAW_ORDER = BODY_PART_NAMES[:-2] + FINGER_PART_NAMES[:2*3] + BODY_PART_NAMES[-2:] + FINGER_PART_NAMES[2*3:]

class Agent:
    
    """
        This class is meant to be a class for my full agent.

        Ideally this should contain all the logic for the body parts of the agent.
        This involves being able to load itself up as well as take in openpose points
        to execute sign animations.
    """

    def __init__ (self, head_img, neck_img, body_img, upperarm_img, forearm_img, hand_img, finger_img):
        """
            Agent Constructor

            Params
            ------
            head_img : str
                path to the head image
            neck_img : str
                path to the neck image
            body_img : str
                path to the body image
            upperarm_img : str
                path to the upper_arm image
            forearm_img : str
                path to the forearm image
            hand_img : str
                path to the hand image (note: the hand image should not contain fingers, just the palm of the hand)
            finger_img : str
                path to the finger image

            Returns
            -------
            Agent
        """

        # A dictionary containing all the body parts of the agent
        self.body_parts = dict()

        self.body_parts["head"] = static_body_part.StaticBodyPart(head_img)
        self.body_parts["neck"] = dynamic_body_part.DynamicBodyPart(neck_img)
        self.body_parts["body"] = static_body_part.StaticBodyPart(body_img, 0.9)
        self.body_parts["upperarm-l"] = dynamic_body_part.DynamicBodyPart(upperarm_img)
        self.body_parts["upperarm-r"] = dynamic_body_part.DynamicBodyPart(upperarm_img)
        self.body_parts["forearm-l"] = dynamic_body_part.DynamicBodyPart(forearm_img)
        self.body_parts["forearm-r"] = dynamic_body_part.DynamicBodyPart(forearm_img)
        self.body_parts["hand-l"] = dynamic_body_part.DynamicBodyPart(hand_img)
        self.body_parts["hand-r"] = dynamic_body_part.DynamicBodyPart(hand_img)

        for finger_id in FINGER_PART_NAMES:
            self.body_parts[finger_id] = dynamic_body_part.DynamicBodyPart(finger_img, scale_x=4.0)


    def draw (self):
        """ Calls the draw function on each body part. """
        for body_part_name in DRAW_ORDER:
            self.body_parts[body_part_name].draw()

    
    def update (self, pose_data):
        """
            Positions the agent into a pose

            Params
            ------
            pose_data : dict
                openpose skeletal keypoints for each body part
        """

        for body_part_name in DRAW_ORDER:
            self.body_parts[body_part_name].update(pose_data[body_part_name])

