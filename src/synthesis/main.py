"""
    This is where my main application will run.
    It will take in arguments, one of which is a text file that has the phrases to sign, separated by new lines.
    It will animate the corresponding sign in the BSL alphabet.
"""

from random import shuffle
from argparse import ArgumentParser
import pyglet
from signal import SignAl



def get_texts (filepath):
    """
        Gets the phrases/texts from a file separating on lines

        Params
        ------
        filepath : str
            file you want to load the phrases from

        Returns
        -------
        list
            sequence of strings that are phrases
    """
    with open(filepath, "r") as file:
        return file.read().splitlines()



if __name__ == "__main__":
    # Arguments
    parser = ArgumentParser()
    parser.add_argument(
        "--model_dir", 
        default="../../human/", 
        help="Directory that includes the human body part models. Must include pngs named: head, neck, body, upperarm, forearm, hand, finger."
    )
    parser.add_argument(
        "--sign_dict", 
        default="../../training-data-keypoints/synthesis.json", 
        help="JSON file that contains openpose keypoints for certain signs."
    )
    parser.add_argument(
        "--resolution", 
        default="720x1280", 
        help="Width x Height of the window you wish to create."
    )
    parser.add_argument(
        "--text_file", 
        default="../../evaluation-text/user.txt", 
        help="The text that should be signed."
    )
    parser.add_argument(
        "--mode", 
        default="sign", 
        help="What mode the program should run in :: sign -> sign all phrases in the text file provided  |  eval -> sign half of the phrases randomly"
    )
    args = parser.parse_known_args()

    # The SignAl instance that contains the agent and logic to animate it
    sign_al = SignAl(args[0].resolution, args[0].model_dir, args[0].sign_dict)


    # Get the phrases and optionally randomise and split them if we are in evaluation mode
    texts = get_texts(args[0].text_file)
    ordering = list(range(1, len(texts) + 1))

    if args[0].mode == "sign":
        virtual_ordering = [x - 1 for x in ordering]
    else:
        shuffle(ordering)

        real_ordering = ordering[:5]
        output_virtual_ordering = ordering[5:]
        virtual_ordering = [x - 1 for x in ordering[5:]]

        print("Human Agent:", real_ordering)
        print("Virtual Agent:", output_virtual_ordering)


    # Represents the text number we are signing
    virtual_text_num = 0


    # The game engine loop of what will be drawn on every frame
    @sign_al.window.event
    def on_draw ():
        sign_al.window.clear()
        sign_al.agent.draw()
        sign_al.label.draw()


    # The key release event handler that handles moving onto the next sign
    @sign_al.window.event
    def on_key_release (symbol, modifiers):
        global virtual_text_num

        if symbol == pyglet.window.key.ENTER and not sign_al.animating:
            if virtual_text_num >= len(virtual_ordering):
                pyglet.app.exit()
            else:
                text = texts[virtual_ordering[virtual_text_num]]
                sign_al.sign_text(text)
                virtual_text_num += 1

                if args[0].mode == "sign":
                    print(text)


    pyglet.app.run()

