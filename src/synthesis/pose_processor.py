"""
    This module will contain functions that will help to process 
    openpose points into a more friendly format for the agent.
"""

import json
import numpy
from scipy.ndimage import convolve1d


def load_sign_dictionary (file_path):
    """
        Loads a json file and returns it as a dictionary.

        Params
        ------
        file_path : str
            the path to the openpose json data

        Returns
        -------
        dict
            the openpose data
    """

    with open(file_path, "r") as json_file:
        return json.load(json_file)



def generate_text_to_sign (text, dictionary, space_frame_delay = 20):
    """
        Converts a piece of text like a sentence into sign language keypoints.

        Params
        ------
        text : str
            The text that will be translated
        dictionary : dict
            sign language dictonary that contains the openpose keypoints for all the character signs
        space_frame_delay : int, optional
            how many frames to delay for a space character
        
        Returns
        -------
        dict
            smoothed animation keypoints that represent the text parameter
    """

    animation_data = {"body": [], "left_hand": [], "right_hand": []}

    text = text.lower()

    if dictionary.get(text[0], None) == None:
        print("ERROR: The text inputted must start with a character that is in the dictionary!")
        exit(-1)

    for char in text:
        char_animation_data = dictionary.get(char, None)
        if char_animation_data:
            animation_data["body"] += char_animation_data[0]["body"]
            animation_data["left_hand"] += char_animation_data[0]["left_hand"]
            animation_data["right_hand"] += char_animation_data[0]["right_hand"]
        else:
            animation_data["body"] += [animation_data["body"][-1]] * space_frame_delay
            animation_data["left_hand"] += [animation_data["left_hand"][-1]] * space_frame_delay
            animation_data["right_hand"] += [animation_data["right_hand"][-1]] * space_frame_delay

    return smooth_sign_data(animation_data, sliding_window_size=15)



def smooth_sign_data (video_data, sliding_window_size = 3):
    """
        Smooths a sequence of sign animation keypoints.

        Params
        ------
        video_data : dict
            sign animation keypoints
        sliding_window_size : int, optional
            the size of the moving average window to be used
        
        Returns
        -------
        dict
            smoothed keypoint data
    """
    body_data = numpy.array(video_data["body"])
    left_hand_data = numpy.array(video_data["left_hand"])
    right_hand_data = numpy.array(video_data["right_hand"])
    
    kernel = numpy.ones(sliding_window_size) / sliding_window_size

    body_data[:,:,:2] = convolve1d(body_data[:,:,:2], kernel, axis=0)
    left_hand_data[:,:,:2] = convolve1d(left_hand_data[:,:,:2], kernel, axis=0)
    right_hand_data[:,:,:2] = convolve1d(right_hand_data[:,:,:2], kernel, axis=0)

    video_data["body"] = body_data.tolist()
    video_data["left_hand"] = left_hand_data.tolist()
    video_data["right_hand"] = right_hand_data.tolist()

    return video_data


def segment_pose (body_data, left_hand_data, right_hand_data):
    """
        Takes a frame of openpose data and separates it into coords for each body part.

        Params
        ------
        body_data : list
            2 dimensional list of openpose body keypoint data
        left_hand_data : list
            2 dimensional list of openpose left hand keypoint data
        right_hand_data : list
            2 dimensional list of openpose right hand keypoint data

        Returns
        -------
        dict
            keys are the body part names and values are the openpose keypoints
            e.g. body_part_name: ((x1, y1), (x2, y2))
    """
    agent_pose = dict()

    # NOTE: currently I am not including the confidence values
    agent_pose["head"] = (tuple(body_data[-2][:2]), tuple(body_data[-1][:2]))
    agent_pose["neck"] = (tuple(body_data[0][:2]), tuple(body_data[1][:2]))
    agent_pose["body"] = (tuple(body_data[2][:2]), tuple(body_data[5][:2]))

    # bodypart-left/right
    agent_pose["upperarm-l"] = (tuple(body_data[2][:2]), tuple(body_data[3][:2]))
    agent_pose["upperarm-r"] = (tuple(body_data[5][:2]), tuple(body_data[6][:2]))
    agent_pose["forearm-l"] = (tuple(body_data[3][:2]), tuple(body_data[4][:2]))
    agent_pose["forearm-r"] = (tuple(body_data[6][:2]), tuple(body_data[7][:2]))
    agent_pose["hand-l"] = (tuple(body_data[4][:2]), tuple(right_hand_data[5][:2]), tuple(right_hand_data[17][:2]), tuple(right_hand_data[5][:2]))
    agent_pose["hand-r"] = (tuple(body_data[7][:2]), tuple(left_hand_data[5][:2]), tuple(left_hand_data[17][:2]), tuple(left_hand_data[5][:2]))
    
    # finger-left/right-base/middle/top
    agent_pose["thumb-l-b"] = (tuple(left_hand_data[1][:2]), tuple(left_hand_data[2][:2]))
    agent_pose["thumb-l-m"] = (tuple(left_hand_data[2][:2]), tuple(left_hand_data[3][:2]))
    agent_pose["thumb-l-t"] = (tuple(left_hand_data[3][:2]), tuple(left_hand_data[4][:2]))

    agent_pose["thumb-r-b"] = (tuple(right_hand_data[1][:2]), tuple(right_hand_data[2][:2]))
    agent_pose["thumb-r-m"] = (tuple(right_hand_data[2][:2]), tuple(right_hand_data[3][:2]))
    agent_pose["thumb-r-t"] = (tuple(right_hand_data[3][:2]), tuple(right_hand_data[4][:2]))

    agent_pose["index-l-b"] = (tuple(left_hand_data[5][:2]), tuple(left_hand_data[6][:2]))
    agent_pose["index-l-m"] = (tuple(left_hand_data[6][:2]), tuple(left_hand_data[7][:2]))
    agent_pose["index-l-t"] = (tuple(left_hand_data[7][:2]), tuple(left_hand_data[8][:2]))

    agent_pose["index-r-b"] = (tuple(right_hand_data[5][:2]), tuple(right_hand_data[6][:2]))
    agent_pose["index-r-m"] = (tuple(right_hand_data[6][:2]), tuple(right_hand_data[7][:2]))
    agent_pose["index-r-t"] = (tuple(right_hand_data[7][:2]), tuple(right_hand_data[8][:2]))

    agent_pose["middle-l-b"] = (tuple(left_hand_data[9][:2]), tuple(left_hand_data[10][:2]))
    agent_pose["middle-l-m"] = (tuple(left_hand_data[10][:2]), tuple(left_hand_data[11][:2]))
    agent_pose["middle-l-t"] = (tuple(left_hand_data[11][:2]), tuple(left_hand_data[12][:2]))

    agent_pose["middle-r-b"] = (tuple(right_hand_data[9][:2]), tuple(right_hand_data[10][:2]))
    agent_pose["middle-r-m"] = (tuple(right_hand_data[10][:2]), tuple(right_hand_data[11][:2]))
    agent_pose["middle-r-t"] = (tuple(right_hand_data[11][:2]), tuple(right_hand_data[12][:2]))

    agent_pose["ring-l-b"] = (tuple(left_hand_data[13][:2]), tuple(left_hand_data[14][:2]))
    agent_pose["ring-l-m"] = (tuple(left_hand_data[14][:2]), tuple(left_hand_data[15][:2]))
    agent_pose["ring-l-t"] = (tuple(left_hand_data[15][:2]), tuple(left_hand_data[16][:2]))

    agent_pose["ring-r-b"] = (tuple(right_hand_data[13][:2]), tuple(right_hand_data[14][:2]))
    agent_pose["ring-r-m"] = (tuple(right_hand_data[14][:2]), tuple(right_hand_data[15][:2]))
    agent_pose["ring-r-t"] = (tuple(right_hand_data[15][:2]), tuple(right_hand_data[16][:2]))

    agent_pose["pinky-l-b"] = (tuple(left_hand_data[17][:2]), tuple(left_hand_data[18][:2]))
    agent_pose["pinky-l-m"] = (tuple(left_hand_data[18][:2]), tuple(left_hand_data[19][:2]))
    agent_pose["pinky-l-t"] = (tuple(left_hand_data[19][:2]), tuple(left_hand_data[20][:2]))

    agent_pose["pinky-r-b"] = (tuple(right_hand_data[17][:2]), tuple(right_hand_data[18][:2]))
    agent_pose["pinky-r-m"] = (tuple(right_hand_data[18][:2]), tuple(right_hand_data[19][:2]))
    agent_pose["pinky-r-t"] = (tuple(right_hand_data[19][:2]), tuple(right_hand_data[20][:2]))


    return agent_pose



