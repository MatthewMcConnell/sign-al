"""
    A module that contains the static body part class
"""

from math import sqrt
import pyglet
import body_part
import signal

class StaticBodyPart (body_part.BodyPart):
    """
        Represents a type of body part that will have a central point.
        The body parts of this type will be positioned such that they rotate
        around a central point.
    """

    def __init__ (self, img_path, anchor_height=None):
        """
            Creates a dynamic body part with the anchor of the sprite being the center of the sprite.

            Params
            ------
            img_path : str
                the file path to the image to be used as the sprite

            anchor_height : float, optional
                a scaling value from 0 to 1 that indicates how where the anchor point should be vertically
                if none is provided then the anchor will be the center

            Returns
            -------
            StaticBodyPart
        """
        super().__init__(img_path)

        self.sprite.image.anchor_x = self.sprite.width / 2

        if anchor_height:
            self.sprite.image.anchor_y = self.sprite.height * anchor_height
        else:
            self.sprite.image.anchor_y = self.sprite.height / 2

    
    def update (self, coords):
        """
            Updates the position and scale of the body part.

            Params
            ------
            coords : tuple
                contains floats that represent (x1, y1, x2, y2)
        """

        x1, y1, x2, y2 = coords[0][0] * signal.WIDTH, (1 - coords[0][1]) * signal.HEIGHT, coords[1][0] * signal.WIDTH, (1 - coords[1][1]) * signal.HEIGHT

        # getting an average between the 2 points for a center point
        mid_x, mid_y = (x1 + x2) / 2, (y1 + y1) / 2

        # getting the scaling factor by finding the magnitude and comparing it to the image width
        scale = sqrt(((x1 - x2) + (y1 - y2)) ** 2) / self.sprite.image.width * 1.25
        
        self.sprite.update(mid_x, mid_y, scale=scale)



    

    
